# Matemáticas
## Conjuntos, Aplicaciones y funciones (2002)
```plantuml
@startmindmap
*[#LightCoral] Matemática de los conjuntos,\n aplicaciones y funciones
	*[#White] Conjunto
		*_ ¿Qué es?
			*[#Pink] No se puede definir
			*[#Pink] Colección
			*[#Pink] Reunión
			*[#Pink] Es intuitivo
		*_ Conceptos primitivos
			*[#Pink] Conjunto y elemento no se define
			*[#Pink] Relación de pertenencia
				*_ Asi que
					*[#PowderBlue] Dado un elemento y un conjunto, \nse puede saber si está el elemento o\n no dentro del conjunto
		*_ Inclusión de conjuntos
			*[#Pink] Noción que hay entre los conjuntos que \nes un trasunto de la relación de orden
			*[#Pink] Un conjunto está incluido en otro
				*_ Tal que
					*[#PowderBlue] Todos los elementos del primero\n pertenecen al segundo conjunto
		*_ Operaciones de \nlos conjuntos
			*[#Pink] Intersección
				*_ Son
					*[#PowderBlue] Elementos que pertenecen\n simultáneamente a ambos
			*[#Pink] Unión
				*_ Son
					*[#PowderBlue] Elementos que pertenecen \nal menos alguno de ellos
			*[#Pink] Complementación
				*_ Son
					*[#PowderBlue] Los que no pertenecen complementario de un\n conjunto que no pertenecen a un conjunto dado
			*[#Pink] Diferencia
				*_ Es
					*[#PowderBlue] La mezcla o resultado de\n hacer varias operaciones
		*[#Pink] Conjunto universal
			*_ Son
				*[#Pink] Conjuntos de referencia en el que ocurre la teoría
		*[#Pink] Conjunto vacío
			*_ Son las
				*[#Pink] Necesidades lógicas para cerrar el \nconjunto que no tiene elementos
		*[#Pink] Representaciones gráficas 
			*_ Ayudan a 
				*[#Pink] Entender los conceptos y a resolver problemas 
			*_ Se representan
				*[#Pink] Mediante dibujos denominados:
					*_ Diagramas de Venn
						*[#PaleGreen] Colección de elementos que forman parte
						*_ Se representan mediante:
							*[#White] Círculos
							*[#White] Óvalos
							*[#White] Líneas cerradas
						*[#PaleGreen] Encierran a los elementos que forman el conjunto
			*_ Las representaciones se deben a 
				*[#Pink] John Venn
					*_ Tal que 
						*[#PowderBlue] Ayudan a comprender intuitivamente\n la posición de las operaciones
			*_ Son útiles para
				*[#Pink] hacerse idea de cómo son las cosas
					*_ De modo que
						*[#PowderBlue] No sirve como método de demostración

		*[#Pink] Cardinal de conjunto
			*_ Son
				*[#Pink] Número de elementos que conforman el conjunto
					*_ Números
						*[#PaleGreen] Naturales 1,2,3.... según los elementos que tengan 
			*_ Propiedades
				*[#PowderBlue] Fórmula para resolver cardinales
					*_ Es
						*[#PaleGreen] Una unión es igual al cardinal de uno\n de los conjuntos, el conjunto A más el cardinal del segundo conjunto\n B menos el cardinal de la intersección
				*[#PowderBlue] Acotación de cardinales
					*_ Basada en 
						*[#PaleGreen] Serie de relaciones
							*[#White] Quien es mayor
							*[#White] Menos
							*[#White] Igual de cardinales de conjuntos
		*_ Historia del cambio
			*[#Pink] Proceso de cambio
		*_ Transformación
			*[#Pink] Identificar qué elementos se\n convierte en el segundo
		*_ Aplicación
			*[#Pink] Es una transformación o regla que convierte a cada uno\n de los elementos del conjunto en único elemento de un conjunto final o destino
			*[#Pink] Cada uno de los elementos del primer conjunto tiene que tener un transformado
		*_ Imagen inversa
			*[#Pink] Transformación de transformación
				*_ Se llama
					*[#Pink] Composición de aplicaciones
				*[#Pink] Obligación primero y obligación a continuación\n mediante la primera aplicación
		*_ Tipos de aplicaciones
			*[#Pink] Aplicación inyectiva
			*[#Pink] Aplicación sobreyectiva
			*[#Pink] Aplicación biyectiva
	*[#White] Función
		*_ Aplicación conjunto de números
			*[#Pink] Fáciles de ver
		*_ Sistema de coordenadas
			*[#Pink] Figura gráfica de la función
				*[#PowderBlue] Serie de puntos
				*[#PowderBlue] Línea recta
				*[#PowderBlue] Parábolas
@endmindmap
```
## Funciones (2010)
```plantuml
@startmindmap
*[#LightCoral] Funciones
	*_ Es 
		*[#Pink] El corazon de las matematicas
	*_ Se centra en 
		*[#Pink] Analisis funcional
		*[#Pink] Analisis matemcatico
	*_ Refleja
		*[#Pink] El pensamiento del hombre que ha hecho 
			*_ para
				*[#Pink] Comprender el entorno
				*[#Pink] Resolver problemas cotidianos
	*[#Pink] El cambio
		*_ Son
			*[#PowderBlue] situaciones que en el momento o circuentacia podrucen resltados diferentes
				*_ Ejemplo
					*[#White] Temperatura en un lugar
					*[#White] Cambio de pesos
		*_ a fin de
			*[#PowderBlue] Conocer como matematizar las matematicas
			*[#PowderBlue] Conocer que matematicas se utilizan
				*[#White] Predecir
				*[#White] Preveer
				*[#White] Conocer
				*[#White] Obtener conociemiento Cientifico del cambio
	*_ Que es una funcion			
		*[#Pink] Es una obligación especial
			*_ de forma tal que
				*[#Pink] Los conjuntos que están relacionado\n son conjunto de números
					*_ Ejemplo
						*[#PowderBlue] Conjunto de números reales
			*_ Ejemplo
				*[#Pink] Funcion que a cada numero le asigna su cuadrado
					*_ de manera que
						*[#PowderBlue] Es una regla que le asigna su cuadrado
	*_ Representaciones gráficas
		*[#Pink] Representación cartesiana
			*_ En honor a 
				*[#PowderBlue] René Descartes
			*_ Representación en un plano
				*[#PowderBlue] En un eje los valores de un conjunto de números reales
				*[#PowderBlue] En el otro eje las de las imágenes
	*[#Pink] Caracteristicas
		*_ Funciones crecientes
			*[#Pink] Aumenta la variable independiente
				*_ Entonces
					*[#PowderBlue] Aumentan sus imágenes
						*_ Ejemplo
							* Al aunmentar las x aumentas sus imagenes
		*_ Funciones decrecientes
			*[#Pink] Necesitamos investigar 
				*_ como son sus
					*[#PowderBlue] Como es la funcion
					*[#PowderBlue] Su comportamiento
						*_ Ejemplo
							*[#Pink] Disminuyen sus variables
							*[#Pink] Al aumentar sus variables
								*[#PowderBlue] Disminuyen las imágenes
	*_ Comportamiento 
		*[#Pink] Máximo
			*[#PowderBlue] Cuando pasa por su máximo valor
		*[#Pink] Mínimos
			*[#PowderBlue] Cuando decrece hasta su mínimo valor
		*_ Ejemplo de minimo y maximo
			*[#PowderBlue] Temperatura en todo un dia
				*[#Pink] Cuando sale el sol aumenta al maximo
				*[#Pink] Despues cuando se pone el sol baja
				*[#Pink] Cuando se hace la noche baja por el punto mas bajo
		*[#Pink] Límites
			*_ Se considera limite cuando
				*[#PowderBlue] Los valores de la función están cerca de sus imágenes
				*[#PowderBlue] Cuando se considera un valor que están cerca del punto\n de sus imágenes de un determinado valor
				*[#PowderBlue] Cuando nos aproximamos a valores del punto de interés
			*_ La idea es
				*[#PowderBlue] Como se aproxima a que valores de una funcion 
					*_ cuando 
						*[#Pink] nos acercamos al punto de interes
		*[#Pink] Continuidad
			*_ Es
				*[#PowderBlue] Una funcion que tiene un buen comportamiento
					*_ Es decir
						*[#Pink] que no produce saltos
						*[#Pink] Ni discontinuidades
					*_ Son
						*[#Pink] Funciones mas manejable
						*[#Pink] Funciones que se comportan mejor
							*_ por eso
								*[#White] Tiene continuidad
							*_ Ejemplo
								*[#White] cuando nos movemos en un punto de interes, los valores estan cercas del punto de origen
	*_ Derivadas
		*[#Pink] Aproximar una función complicada\n con una función más simple
			*_ como
				*[#White] Funcion mas sencilla y en punto dado sirva aproximacion de la funcion de interes
					*_ Son
						*[#PowderBlue] Funciones cuya grafica son lineales
							*[#White] De la forma f(x)=x+b
		*[#Pink] Función más sencilla son las funciones\n donde su gráfica son líneas rectas
			*_ Son
				*[#PowderBlue] Funciones lineales
					*[#White] Su pendiente son las derivadas
		*[#Pink] ¿Qué es lo que hace?
			*[#PowderBlue] Resolver el problema de la aproximación de una función\n compleja mediante una función lineal (simple)
		*[#Pink] Cálculos
			*_ Reglas
				*[#PowderBlue] Derivada de un producto	
				*[#PowderBlue] Derivada de un exponente 
					*[#White] x^2
@endmindmap
```
## La matemática del computador (2002)
```plantuml
@startmindmap
*[#LightCoral] aritmética del computador
	*[#LightPink] Las matematicas
		*_ ayudan
			*[#PowderBlue] Al mundo de los computadores
		*_ se encuentran en
			*[#PowderBlue] los funcionamiento de los aparatos electronicos
	*_ Aritmetica finita
		*[#LightPink]  Probelmas de tipo matematico practicos
			*_ como 
				*[#White] numero infinito de posiciones
				*[#White] y los numeros reales pierden el sentido
	*_ Cálculos
		*[#LightPink] Números abstractos o teóricos
		*[#LightPink] Números reales 
			*_ Ejemplo
				*[#PowderBlue] raíz (2)
		*[#LightPink] Número decimales
			*[#PowderBlue] infinitas cifras
		*[#LightPink] Número fraccional
			*_ Ejemplo
				*[#PowderBlue] 1/3
		*[#LightPink] Números fraccionarios
		*[#LightPink] Número aproximado
		*[#LightPink] Error
			*[#PowderBlue] Se come al aproximar número real \npor un número infinito de cifras
		*[#LightPink] Dígitos significativos
			*_ Que son
				*[#PowderBlue] Son los números o dígitos que proporcionan\n información sobre el signo
	*_ Truncar
		*_ Es
			*[#PowderBlue] Cortar a un número con infinitas cifras decimales a la derecha
				*_ Ejemplo
					*[#LightPink] Si hay 8 posiciones para representar un numero como
						*[#White] 3.14159126 hasta aqui seria un aproximacion fiable
		*_ Consiste en
			*[#PowderBlue] despreciar un termino de numero de cifras
	*_ Redondear
		*_ Es:
			*[#PowderBlue] Es un truncamiento refinado
				*[#White] tratar que el error sea lo mas puqueño posible
			*[#PowderBlue] Tipos de errores
				*[#White] Error por exceso
				*[#White] Error por defecto
			*[#PowderBlue] Tiene reglas para poder redondear\n correctamente
		*_ Consiste en
			*[#PowderBlue] despreciar y retocar la ultima cifra
		*_ Pretende
			*[#PowderBlue] hacer esta tecnica con muy pequeño error
	*_ Sistema de numeración
		*[#LightPink] Sistema binario
			*[#PowderBlue] Sistema que utiliza el computador
			*[#PowderBlue] Sistema más simple
			*[#PowderBlue] Sistema en base 2
				*_ como
					*[#White] 0 y 1
			*[#PowderBlue] Operaciones
				*_ Adición binaria
					*[#LightPink] Sumar en base 2
						*_ como tal se hace
							*[#White] Con la misma logica
							*[#White] Adaptando los numeros		
			*_ Como se representa
				*[#LightPink] Mediante bits
				*[#LightPink] Digitos binarios
					*_ como
						*[#White] 0 y 1		
		*_ Se construyen 
			*[#PowderBlue] Imágenes
			*[#PowderBlue] Videos
			*[#PowderBlue] Digitalización
				*_ Ejemplo
					*[#White] Espectros digitales
		*[#LightPink] Sistema octal
			*[#PowderBlue] Tienen representaciones compactas de numeros
			*[#PowderBlue] Son muy empleadas en informatica
		*[#LightPink] Sistema hexadecimal
		*_ Tipo de representaciones
			*[#PowderBlue] interna de numeros enteros
			*[#PowderBlue] en magnitud signo
			*[#PowderBlue] en exceso
			*[#PowderBlue] en complemento 2
			*_ Que sirven para
				*[#PowderBlue] converir un paso e corriente en posiciones de memoria
					*_ tal que se convierte en 
						*[#White] Numero entero 
		*[#LightPink] Aritmética en punto flotante
			*[#PowderBlue] Como se puede sumar, multiplicar\n números en una notación científica
		*[#LightPink] Desbordamiento
	*_ Las matemáticas
		*_ Para que sirve
			*[#LightPink] Es una herramienta imprescindible para el\n desarrollo de los computadores
			*[#LightPink] Ayudan a hacer una representacion habil de\n un numero con infinitas cifras decimales
	*_ como se manejan los\n numeros en la computadora
		*[#PowderBlue] Se representar numeros
			*[#LightPink] Muy pequeños, Muy grandes
				*_ en forma de
					*[#PowderBlue] producto de un numero por una potencia de 10
						*_ que se llama 
							*[#White] Mantisa
@endmindmap
```